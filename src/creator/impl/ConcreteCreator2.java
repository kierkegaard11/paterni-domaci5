/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creator.impl;

import static abstractFactory.AbstractFactory.LBL_TEXT_BIRTHDATE;
import static abstractFactory.AbstractFactory.LBL_TEXT_FIRST_NAME;
import static abstractFactory.AbstractFactory.LBL_TEXT_GENDER;
import static abstractFactory.AbstractFactory.LBL_TEXT_LAST_NAME;
import component.fields.AbstractPanelInput;
import component.fields.PanelInputMultipleTextFields;
import component.fields.PanelInputRadioButton;
import component.fields.PanelInputText;
import creator.Creator;

/**
 *
 * @author maja
 */
public class ConcreteCreator2 extends Creator {

    @Override
    public AbstractPanelInput createPanelInputFirstName() {
        return new PanelInputText(LBL_TEXT_FIRST_NAME);
    }

    @Override
    public AbstractPanelInput createPanelInputLastName() {
        return new PanelInputText(LBL_TEXT_LAST_NAME);
    }

    @Override
    public AbstractPanelInput createPanelInputGender() {
        return new PanelInputRadioButton(LBL_TEXT_GENDER);
    }

    @Override
    public AbstractPanelInput createPanelInputBirthdate() {
        return new PanelInputMultipleTextFields(LBL_TEXT_BIRTHDATE);
    }

    @Override
    public String toString() {
        return "Configuration 2";
    }
}
