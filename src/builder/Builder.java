/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import component.fields.AbstractPanelInput;
import view.FrmPerson;

/**
 *
 * @author maja
 */
public abstract class Builder {

    protected AbstractPanelInput inputFirstName;
    protected AbstractPanelInput inputLastName;
    protected AbstractPanelInput inputGender;
    protected AbstractPanelInput inputBirthdate;

    public abstract void createPanelInputFirstName();

    public abstract void createPanelInputLastName();

    public abstract void createPanelInputGender();

    public abstract void createPanelInputBirthdate();

    public FrmPerson buildForm() {
        return new FrmPerson(inputFirstName, inputLastName, inputGender, inputBirthdate);
    }

}
