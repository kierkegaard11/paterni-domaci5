/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder.impl;

import static abstractFactory.AbstractFactory.LBL_TEXT_BIRTHDATE;
import static abstractFactory.AbstractFactory.LBL_TEXT_FIRST_NAME;
import static abstractFactory.AbstractFactory.LBL_TEXT_GENDER;
import static abstractFactory.AbstractFactory.LBL_TEXT_LAST_NAME;
import builder.Builder;
import component.fields.PanelInputComboBox;
import component.fields.PanelInputMultipleComboBoxes;
import component.fields.PanelInputText;

/**
 *
 * @author maja
 */
public class ConcreteBuilder3 extends Builder {

    @Override
    public void createPanelInputFirstName() {
        inputFirstName = new PanelInputText(LBL_TEXT_FIRST_NAME);
    }

    @Override
    public void createPanelInputLastName() {
        inputLastName = new PanelInputText(LBL_TEXT_LAST_NAME);
    }

    @Override
    public void createPanelInputGender() {
        inputGender = new PanelInputComboBox(LBL_TEXT_GENDER);
    }

    @Override
    public void createPanelInputBirthdate() {
        inputBirthdate = new PanelInputMultipleComboBoxes(LBL_TEXT_BIRTHDATE);
    }

    @Override
    public String toString() {
        return "Configuration 3";
    }
}
